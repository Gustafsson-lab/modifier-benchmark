rm(list = ls())
data_name <- "Meth_MS_PBMC"
run_dir <- getwd()
set_dir <- paste(run_dir , data_name , sep = "/")
dir.create(set_dir, recursive = T)
.libPaths(c(.libPaths() , "/proj/lassim/users/x_tejve/disease_modules/rlibrary/"))
setwd(set_dir)
source(paste(run_dir , "scripts/standard/platform_script_cluster.R" , sep="/"))
##############################################################################################
library(MODifieR)
library(igraph)
library(plyr)
library(data.table)
###############################################################################################
gse_macro <- readRDS("/proj/lassim/users/x_tejve/disease_modules/meth_data/MS_PBMC_meth_input.RDS")
#mods <- Module_generator_func(macro=gse_macro, dataset_name = data_name )
Diamond_module <- readRDS( file = paste(data_name ,"diamond.rds" , sep = "_" ))
mcode_module <- readRDS(file = paste(data_name ,"mcode.rds" , sep = "_" ))
MODA_module <- readRDS(file = paste(data_name ,"MODA.rds" , sep = "_" ))
clique_sum_module <- readRDS(file = paste(data_name ,"clique_sum.rds" , sep = "_" ))
cor_cliq_module <- readRDS(file = paste(data_name ,"correlation_clique.rds" , sep = "_" ))
MD_module <- readRDS( file = paste(data_name ,"module_discoverer.rds" , sep = "_" ))
wgcna_module <- readRDS( file = paste(data_name ,"WGCNA.rds" , sep = "_" ))
DiffCoex_module <- readRDS( file = paste(data_name ,"DIffCoEx.rds" , sep = "_" ))
module_list <- list(clique_sum_module, 
                    cor_cliq_module, 
                    Diamond_module, 
                    mcode_module, 
                    MODA_module, 
                    MD_module, 
                    wgcna_module ,
                    DiffCoex_module)
max_frequency <- get_max_frequency(module_list)
consensus_modules <- lapply(X = 1:max_frequency, FUN = create_module_set, 
                            module_list = module_list)
full_mod_list <- list("Clique_sum" = clique_sum_module$module_genes, 
                      "Cor_cliq" = cor_cliq_module$module_genes, 
                      "Diamond" = Diamond_module$module_genes, 
                      "MCODE" = mcode_module$module_genes, 
                      "MODA" = MODA_module$module_genes, 
                      "Module_discoverer" =  MD_module$module_genes, 
                      "WGCNA" = wgcna_module$module_genes,
                      "DiffCoex"= DiffCoex_module$module_genes)
for (i in 1:length(consensus_modules)) {
  full_mod_list[[paste("consensus" , i , sep="_")]] <- consensus_modules[[i]]$module_genes
}
print_super_module(full_mod_list , output_file = paste("modules" , data_name , "meth.txt" , sep = "_"))
print("file is created")
modules <- paste(normalizePath(dirname(paste("modules" , data_name , "meth.txt" , sep = "_"))) , "/" , paste("modules" , data_name , "meth.txt" , sep = "_") , sep = "")

gwas <- paste(run_dir , "PASCAL/gwas/EUR.WTCCC2_MS_official.txt" , sep = "/")
#modules <- paste(getwd() , list.files(pattern = data_name) , sep = "/")
pascal_command_func(gwas = gwas , modules = modules , file_name = data_name)
print(paste("Module generation and GWAS validation complete for" , data_name , "is completed" , sep = " "))
